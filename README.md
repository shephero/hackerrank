### What is this repository for? ###

This repository hosts my solutions to problems from [HackerRank](https://www.hackerrank.com)

### How do I test the Haskell code? ###

Assuming a unix-like system with ghc: 

```
#!bash
cd /path/to/haskell/file
ghc fileName.hs -o outputFile
./outputFile
```

### How do I test the c++ code? ###

Assuming a unix-like system with g++: 

```
#!bash
cd /path/to/cplusplus/file
g++ fileName.cpp -o outputFile
./outputFile
```

### Disclaimer ###

This code was produced to solve the problem set, and do nothing more. Once it worked, I stopped typing. That means that it might be uglier than I would like. It does, however, pass all the test cases on the site.